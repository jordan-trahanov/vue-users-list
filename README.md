## Production build available at:

[https://jordan-trahanov.gitlab.io/vue-users-list/](https://jordan-trahanov.gitlab.io/vue-users-list/)

# vue-users-list

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
